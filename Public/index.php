<?php
/* ----------
* ! NoCopyright, NoCopyleft for a free world !
* ! PasDeCopyright, PasDeCopyleft pour un monde libre !
* ----------
* Copyright (C) [2016] [Kara.Adrien]   <adrien@iglou.eu>
* ----------
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* ----------
* http://www.apache.org/licenses/LICENSE-2.0
* ----------
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* ----------
* Alpha 0.0.1 - 7 Oct 2016
** ---------- */
// check php vers

// Define the absolute path
define('APP_ROOT', dirname(__DIR__).'/');

define('LIB_DIR', APP_ROOT.'Library/');
define('INC_DIR', APP_ROOT.'Include/');
define('DATA_DIR', APP_ROOT.'Data/');

define('BUFF_DIR', '/tmp/tmpfs_cms/');

define('MASK', '0700');

// Load every componants
require INC_DIR.'require.inc.php';

// CMS Use
main\init();
main\header();
main\start();
