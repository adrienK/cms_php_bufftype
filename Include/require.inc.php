<?php

# Libs
require LIB_DIR.'LittleSecureLib.php';

# Functions
require INC_DIR.'input_output.inc.php';

# Config
require INC_DIR.'config.inc.php';

# Main
require INC_DIR.'main.inc.php';
