<?php
namespace main;

function init(): void
{
    clearstatcache();

    \item\mkdir(BUFF_DIR);
    \conf\init();

    if (gc_enabled()) {
        gc_collect_cycles();
        gc_disable();
    }
}

function header(): void
{
    $conf = \conf\get('https', 'host', 'charset', 'status');
    // Check HTTPS
    if ($conf['https'] &&
        (empty($_SERVER["HTTP_HTTPS"]) || 
        strtolower($_SERVER["HTTP_HTTPS"]) !== "on")
    ) {
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: '.$conf['host'].substr($_SERVER['REQUEST_URI'], 1));
    }

    //header('Content-Type: text/html;charset='.$conf['charset']);

    // Init session
    if ($conf['https']) {
        sessionStart(true);
    } else {
        sessionStart();
    }

    if (!isset($_SESSION['status']) || (empty($_SESSION['status']) && 0 !== $_SESSION['status']))
        $_SESSION['status'] = $conf['status']['doe'];

    // DEBUG REMOVE, JUST FOR TESTS
    $_SESSION['status'] = $conf['status']['root'];

    // include admin parts
    if ($conf['status']['webm'] >= $_SESSION['status'])
        require INC_DIR.'manage_func.inc.php';

    if ($conf['status']['root'] === $_SESSION['status'])
        require INC_DIR.'admin_func.inc.php';
}

function start(): void
{
    //$conf = \conf\get('pageDef');
    //page demandé

    if (isset($_GET['manage']) && function_exists('\manage\main'))
        echo \manage\main();
}
