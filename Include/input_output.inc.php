<?php
namespace item {

function is_readable(string $item, bool $is_file = false): bool
{
    if (!\is_readable($item) || ($is_file && !is_file($item)) || (!$is_file && is_file($item)))
        return (false);

    return (true);
}

function is_writable(string $item): bool
{
    return (\is_writable($item));
}

function exist(string $item): bool
{
    if (!file_exists($item))
        return (false);

    return (true);
}

function get_content(string $item, bool $mod_check = false): string
{
    if ($mod_check && !compare_perms($item, MASK)) {
        remove($item);
        exit();
    }

    return (file_get_contents($item));
}

function push_content(string $item, string $data, bool $add = false): bool 
{
    if ($add)
        $flag = FILE_APPEND | LOCK_EX;
    else 
        $flag = LOCK_EX;

    return (file_put_contents($item, $data, $flag));
}

function compare_perms(string $item, int $expect): bool
{
    if ($expect === (fileperms($item) & 0777))
        return (true);
    else
        return (false);
}

function chmod(string $item, int $perm): bool
{
    return (\chmod($item, $perm));
}

function mkdir(string $item, bool $recursiv = true): bool
{
    if (!\item\exist($item))
        return (\mkdir($item, MASK, $recursiv));
    else
        return true;
}

function rm(string $item): bool
{
    return (unlink($item));
}

function rm_dir(string $item): bool
{
    return (rmdir($item));
}

function rm_r() {}

function encode(array $item): string
{
    return (\json\encode($item));
}

function decode(string $item): array
{
    return (\json\decode($item));
}

function cp(string $item, string $dest): boolean 
{
    return (copy($item, $dest));
}

}

namespace buffer {

function push(string $file, array $datas): string
{
    $source = DATA_DIR.$file;
    $target = BUFF_DIR.$file;

    if (
        (\item\exist($source) && !\item\is_readable($source, true)) ||
        (\item\exist($target) && \item\rm($target))
    ) {
        exit(); // make clean exit
    }

    $datas  = \item\get_content($source);
    \item\push_content(BUFF_DIR.$file, $datas);

    return ($datas);
}

function pull(string $file, array &$datas): void
{
    if (\item\exist(BUFF_DIR.$file) && \item\is_readable(BUFF_DIR.$file, true))
        $datas = \item\get_content(BUFF_DIR.$file);
    else 
        $datas = push($file, $datas);
}

}

namespace json {

function decode(string $content): array
{
    $content = json_decode($content, true);

    if (json_last_error())
        exit('Error: '.json_last_error());

    return ($content);
}

function encode(array $content): string
{
    $content = json_encode(
        $content,
        JSON_HEX_APOS | JSON_NUMERIC_CHECK | JSON_PRESERVE_ZERO_FRACTION
    );

    return ($content);
}

}

namespace conf {

/* *
 * Configuration core, no direct requests
 *
 * @param int $action
 *  requested action 1/2
 *
 * @param array $args
 * 
 * @return array
 * */
function core(int $action, array $args = array()): array
{
    static $datas = array();

    if (empty($datas)) {
        \buffer\pull('config', $datas);
        $datas = \item\decode($datas);
    }

    if (1 === $action) {
        return ($datas);
    } else if (2 === $action) {
        $datas = array_merge($datas, $args);
        return ($args);
    }

    return (array());
}

/* *
 * Initialise configuration
 *
 * @return void
 * */
function init(): void
{
    core(0);
}

/* *
 * Get some configuraton datas
 *
 * @param string *
 *  name of required configuration 
 *
 * @return array
 * */
function get(): array
{
    $datas = core(1);

    if (0 >= func_num_args())
        return $datas;

    $buff  = array();
    $args  = func_get_args();
    
    foreach ($args as &$arg)
        $buff[$arg] = $datas[$arg];

    return ($buff);
}

/* *
 * Set or change configuration
 *
 * @param array $args
 *  'name_of_config' => 'new value'
 *
 * @return array
 * */
function set(array $args): array
{
    return (core(2, $args));
}
}

namespace render {

function page() {}

function menu(array $list): string
{
    $buff = '';

    foreach ($list as $name => $datas) {
        if (!isset($datas['href']) || !isset($datas['content']))
            continue;

        $buff .= '<li id="'.$name.'"><a href="'.$datas['href'].'"';

        if (isset($datas['title']))
            $buff .= ' title="'.$datas['title'].'"';

        if (isset($datas['id']))
            $buff .= ' id="'.$datas['id'].'"';

        if (isset($datas['class']))
            $buff .= ' class="'.$datas['class'].'"';

        if (isset($datas['target']))
            $buff .= ' target="'.$datas['target'].'"';

        $buff .= '>'.$datas['content'].'</a></li>'."\n";
    }

    return ('<ul>'."\n".$buff.'</ul>');
}
}
