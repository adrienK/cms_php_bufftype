<?php
namespace manage {

function main(): string 
{
    $html    = '';
    $jail    = '\manage\pages\\';
    $menu    = \render\menu(menu());
    $request = securityParser($_GET['manage']);
    $function= $jail.$request;

    if (function_exists($function))
        $html .= $function();
    else
        echo 'etrange';
    // Main 
    // logi
    // * Connections
    // * Ajout/sup/config
    //root// configurer site
    //root// * Nom/descrip/motskey/imgfond/logo
    //root// * securite site
    //root// * base serveur/url/langues... 
    //% gerer pages
    //  * Vue comme dans un filemanager
    //  * les pages ont un petit drapeau de langue, transparent si langue
    //  * indisponible
    // gestion menu
    // * ajouter menu
    // ** gere entres du menu
    //root// gerer users
    //root// * ajouter user
    //root// ** nom/mail/passwd/accreditation
    //% gestion media
    // * vue dossier

    return ($menu.$html);
}

function menu(): array
{
    $menu = array(
        'main'   => array('content'=>'accueil','id'=>'','class'=>'','title'=>'','href'=>'?manage=accueil'),
        'pages'  => array('content'=>'pages','id'=>'','class'=>'','title'=>'','href'=>'?manage=pages'),
        'menus'  => array('content'=>'menu','id'=>'','class'=>'','title'=>'','href'=>'?manage=menu'),
        'medias' => array('content'=>'media','id'=>'','class'=>'','title'=>'','href'=>'?manage=media'),
        'logs'   => array('content'=>'logs','id'=>'','class'=>'','title'=>'','href'=>'?manage=logs'),
        'users'  => array('content'=>'users','id'=>'','class'=>'','title'=>'','href'=>'?manage=users'),
        'config' => array('content'=>'config','id'=>'','class'=>'','title'=>'','href'=>'?manage=conf')
    );

    if (function_exists('\admin\menu'))
        $menu += \admin\menu();

    return ($menu);
}

function pages_hf(string $html): string
{
    $header = '';

    $footer = '';

    return ($$html);
}
}

namespace manage\pages {

function pages(): string
{
    $content = '';

    return ($content);
}

function conf(): string
{
    $conf    = \conf\get();
    $content = '<h1>Comics CMS configuration</h1>
                <form action="" method="post">';

    if ($conf['https'])
        $conf['https'] = 'checked';

    // SITE
    $content .= '<fieldset><legend>Base</legend>
                    <label for="https">HTTPs: </label><input id="https" type="checkbox" name="https" value="true" '.$conf['https'].'><br />
                    <label for="url">HOST: </label><input id="url" type="text" name="url" placeholder="www.mywebsite.com" value="'.$conf['host'].'" required><br />
                 </fieldset>
                ';

    // RS MOTOR 
    $content .= '<fieldset><legend>Informations</legend>
                    <label for="title">Title: </label><input id="title" type="text" name="title" placeholder="My web site name" value="'.$conf['title'].'" required><br />
                    <label for="rskey">KeyWord: </label><input id="rskey" type="text" name="rskey" placeholder="web,site,private" value="'.$conf['keyword'].'" required>
                 </fieldset>
                ';
 
    // ROOT 
    $content .= '<fieldset><legend>Root</legend>
                    <label for="rmail">Root user mail: </label><input id="rmail" type="text" name="rmail" placeholder="root@mywebsite.com" value="'.$conf['rootmail'].'" required><br />
                 </fieldset>
                ';
   
    $content .= '<input type="submit" value="Save this configuration"></form>';

    return ($content);
}
}
